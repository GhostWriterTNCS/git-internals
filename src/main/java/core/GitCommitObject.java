package core;

public class GitCommitObject {
	String repoPath;
	String masterCommitHash;

	public GitCommitObject(String repoPath, String masterCommitHash) {
		this.repoPath = repoPath;
		this.masterCommitHash = masterCommitHash;
	}

	public String getRef() {
		return repoPath + "/objects/" + masterCommitHash.substring(0, 2) + "/" + masterCommitHash.substring(2);
	}

	public String getHash() {
		return masterCommitHash;
	}

	public String getProperty(String p) {
		String s = FileIO.readUnzipFile(getRef());
		if (s.isEmpty() || !s.contains(p + " ")) {
			return "";
		}
		s = s.substring(s.indexOf(p + "") + p.length() + 1);
		if (!s.contains("\n")) {
			return s;
		}
		return s = s.substring(0, s.indexOf("\n"));
	}

	public String getTreeHash() {
		return getProperty("tree");
	}

	public String getParentHash() {
		return getProperty("parent");
	}

	public String getAuthor() {
		String s = getProperty("author");
		if (s.isEmpty() || !s.contains("> ")) {
			return "";
		}
		return s.substring(0, s.indexOf("> ") + 1);
	}

}
