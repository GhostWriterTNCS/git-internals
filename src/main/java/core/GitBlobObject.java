package core;

public class GitBlobObject {
	String repoPath;
	String id;

	public GitBlobObject(String repoPath, String id) {
		this.repoPath = repoPath;
		this.id = id;
	}

	public String getRef() {
		return repoPath + "/objects/" + id.substring(0, 2) + "/" + id.substring(2);
	}

	public String getType() {
		String s = FileIO.readUnzipFile(getRef());
		if (s.isEmpty() || !s.contains(" ")) {
			return "";
		}
		return s.substring(0, s.indexOf(" "));
	}

	public String getContent() {
		String s = FileIO.readUnzipFile(getRef());
		if (s.isEmpty() || s.indexOf((char) 0) < 0) {
			return "";
		}
		return s.substring(s.indexOf((char) 0) + 1);
	}

}
