package core;

public class GitRepository {
	String repoPath;

	GitRepository(String repoPath) {
		this.repoPath = repoPath;
	}

	public String getHeadRef() {
		String headRef = FileIO.readFile(repoPath + "/HEAD").trim();
		headRef = headRef.substring(headRef.indexOf("ref: ") + 5);
		return headRef;
	}

	public String getRefHash(String ref) {
		return FileIO.readFile(repoPath + "/" + ref).trim();
	}
}
