package core;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.zip.InflaterInputStream;

public class FileIO {
	public static String readFile(String path) {
		byte[] encoded;
		try {
			encoded = Files.readAllBytes(Paths.get(path));
			return new String(encoded);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String readUnzipFile(String path) {
		byte[] encoded;
		try {
			FileInputStream fis = new FileInputStream(path);
			@SuppressWarnings("resource")
			InflaterInputStream inflate = new InflaterInputStream(fis);
			encoded = new byte[(int) fis.getChannel().size() + 3];
			inflate.read(encoded);
			return new String(encoded);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

}
